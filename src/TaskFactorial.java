import java.math.BigInteger;

public class TaskFactorial {
    public static void main(String[] args) {
        BigInteger factorialOfTwenty = factorial(20);
        System.out.println(factorialOfTwenty);
    }

    private static BigInteger factorial(int n) {
        if (n < 0) { //тут можно было бы бросить исключение, но для простоты просто напишем сообщение
            System.out.println("Нельзя вычислить факториал отрицательного числа");
            return BigInteger.valueOf(-1);
        } else if (n <= 1) {
            return BigInteger.valueOf(1);
        } else {
            return BigInteger.valueOf(n).multiply(factorial(n - 1));
        }
    }
}
