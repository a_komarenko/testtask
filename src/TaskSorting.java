import java.io.*;
import java.util.*;

public class TaskSorting {
    public static void main(String[] args) {
        try {
            FileInputStream stream = new FileInputStream("src/example");
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

            String line = reader.readLine();

            String[] list = line.split(",");

            Arrays.sort(
                    list,
                    (s1, s2) -> sort(s1, s2, Order.ASC)
            );
            System.out.println(
                    String.join(",", list)
            );

            Arrays.sort(
                    list,
                    (s1, s2) -> sort(s1, s2, Order.DESC)
            );
            System.out.println(
                    String.join(",", list)
            );

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    enum Order {ASC, DESC}

    private static int sort(String s1, String s2, Order order) {
        int i1 = Integer.parseInt(s1);
        int i2 = Integer.parseInt(s2);
        if (order == Order.DESC) {
            return i2 - i1;
        } else if (order == Order.ASC) {
            return i1 - i2;
        }
        return 0;
    }
}
